'use strict';
const arrayutils = require('../commons/arrayutils.js');

/**
* @author  Juan Azocar Malverde
* @date    05-12-2017
* @version 1.0.0
* @description This class provide a service layer for user authentication and 
*              others actions (logout, change password.. )
*/
class loginservice {
   
  constructor(hash) {
	this.sessions = [];
    this.users = [];
    this.passwords = [];
    this.arrayUtil = new arrayutils();
    
    Object.keys(hash).map(k => ({k, v: hash[k]})).map(e => {
      this.users = this.users.concat([e.k]);
      this.passwords = this.passwords.concat([e.v]);
    });
  } 
  
  /**
  * Logout method for users
  * @param user
  * @description This method turns to null the user passed by parameter
  */
  logoutUser(user) {
	 if(user == undefined || user == '')
	 {
		 //Do something
		 console.log('The user can not be empty') 
	 }
	 else
	 {
		 this.sessions.forEach((session, i) => {
		      if (session === user) {
		        this.sessions[i] = null;
		      }
		    });
		    this.sessions = this.sessions.filter(session => session !== null);
		    console.log('User '+user+' logout session.');
	 }
    
  }
  
  
  /**
  * UserExist method
  * @param user
  * @description This method checks if user exists in array users[]
  */
  verifyUser(user) {
	  if(user == undefined || user == '')
	  {
		 //Do something
		 console.log('The user can not be empty');
		 return false;
	  }
	  else
	  {
		  let temp = '';
		    for (let i of this.users) {
		      if (i === user) {
		        temp = user;
		      }
		    }
		    let exists = (temp !== '' && temp === user);
		    exists ? console.log('User '+user+' exists'): console.log('User '+user+' doesnt exists');
		    return exists;	 
	  }
    
  }

  /**
  * Login method for users
  * @param user
  * @param password
  * @description This method allow to users authenticate
  */
  loginUser(user, password) {
    let index = this.arrayUtil.getElementIndex(user, this.users);
    if (this.passwords[index] === password) {
      this.sessions.push(user);
      console.log('The user '+user+' has been authenticated');
    }
    else
    {
     console.log('User '+user+' not found');	
    }    	
  }
  
 /**
  * Remove method for users
  * @param user
  * @description This method remove a user from the array. 
  */
  removeUser(user) {
	  
	  if(user == undefined || user == '')
	  {
		 //Do something
		 console.log('The user can not be empty') 
	  }
	  else
	  {
		let index = this.arrayUtil.getElementIndex(user, this.users);    
	    this.users[index] = null;
	    this.passwords[index] = null;
	    this.users = this.users.filter(user => user !== null);
	    this.passwords = this.passwords.filter(password => password !== null);
	    console.log('The user '+user+' was removed.');
	  }   
    
  }
  
  /**
  * Update password method for users
  * @param user
  * @param oldPassword
  * @param newPassword
  * @description This method change the old password for the new password
  *              for the user passed by parameter only if exist in users array
  */ 
    changePassword(user, oldPassword, newPassword) {
    // First we check if the user exists
    let user1 = '';
    for (let i of this.users) {
      if (i === user) {
        user1 = user;
      }
    }
    if (user1 === user) {
      let index = this.arrayUtil.getElementIndex(user, this.users);
      if (this.passwords[index] === oldPassword) {
        this.passwords[index] = newPassword;
        return true;
      }
    }
    console.log('The password for the user '+user+' was updated.')
    return false;
  }

   /**
    * Check password for users
    * @param user
    * @param password
    * @return boolean passswordCorrect
    * @description This method verify the passed password for the user
    */  
    checkPassword(user, password) {
      if(user == undefined || user == '' || password == undefined || password == '')
   	  {
   		 //Do something
   		 console.log('The user/password can not be empty');
   		 return false;
   	  }
   	  else
   	  {
   		let index = this.arrayUtil.getElementIndex(user, this.users);
    	let passwordCorrect = this.passwords[index] === password;
    	passwordCorrect ?  console.log('The password for the user '+user+' is correct.') :  console.log('The password for the user '+user+' isnt correct.')
    	return passwordCorrect;
   	  }
    	
  }
  
  
  /**
    * Register users method
    * @param user
    * @param password
    * @description This method register a user/password passed by parameter
    */ 
  registerUser(user, password) {
	  if(user == undefined || user == '' || password == undefined || password == '')
	  {
		 //Do something
		 console.log('The user/password can not be empty') 
	  }
	  else
	  {
		  let lastIndex = this.users.length;
		  this.users[lastIndex] = user;
		  this.passwords[lastIndex] = password;
		  console.log('The user '+user+' was registered.');
	  }
   
  }
	
};
module.exports = loginservice;