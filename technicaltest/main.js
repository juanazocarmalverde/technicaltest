'use strict';
//We need to instance of service layers
const loginservice = require('./service/loginservice.js');

//We must change to REST Service call ....
var fs = require('./users.json');
let registeredUsers  = JSON.stringify(fs);

//Call to LoginService constructor.
let login = new loginservice(registeredUsers);

//We do some calls
login.registerUser('user4', 'pass4');
login.loginUser('user4', 'pass4');
login.verifyUser('user4');
login.changePassword('user3', 'pass3', 'pass5');
login.loginUser('user3', 'pass5');
login.logoutUser('user4');
login.logoutUser('user3');
