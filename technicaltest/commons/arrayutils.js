'use strict';

/**
* @author  Juan Azocar Malverde
* @date    05-12-2017
* @version 1.0.0
* @description This class provides common methods for manage arrays
*/
class arrayutils {	
	
	/**
	* @name getElementIndex
	* @param element
	* @param array[]
	* @return int index
	* @description This method allow to know the index of an element in array
	*/ 
  getElementIndex(element, array) {
	let index=0;
    for (let i of array) {
      if (i === element) {
        return index;
      }
      index += 1;
    }
    return index;
  }
	
};

module.exports = arrayutils;